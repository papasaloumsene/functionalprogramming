package excerciceFonctionalGrogramme;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class ExcerciceFunctionProgramming {
	
public static void main(String[] args) {
	
   List<Integer> number = List.of(12,32,365,9);
	//Collection<Integer> number2 = List.of(1,2,3,4,5,6,7,8,9,10);
	int sum =addNumber(number);
	System.out.println(sum);
	
	
	//List<String> cources = List.of("spring boot","api","angular","java","php","spring batch","reactjs");
	
	//Collection<Integer> employee = List.of(1421,21612,763,317,9063);
	
	
	//value.stream().forEach(System.out::println);
	
	//employee.stream().filter(salary -> salary.min(salary, salary) <= 1421 ).forEach(System.out::println);
	

	//value.stream().filter(a -> a.contains("spring")).forEach(System.out::println);
	
	//value.stream().filter(c -> c.length() ==3).forEach(System.out::println);
	//number2.stream().map(numb -> numb *4).forEach(System.out::println);
	
	//cources.stream().map(c-> c +" "+c.length()).forEach(System.out::println);
	
   //number.stream().map(n -> n * n).forEach(System.out::println);

//  private static void printEventNumber(Collection<Integer> number) {
////
////	number.stream()
////	.filter(num -> num%2==0)
////	.forEach(System.out::println);

	
}

    private static int add(int a, int b) {
    	
    	return a+b;
    }
//    private static int addNumber(List<Integer> number) {
//	
//	return number.stream().reduce(0,ExcerciceFunctionProgramming::add);
//}
	
//    private static int addNumber(List<Integer> number) {
//    	
//    	return number.stream().reduce(0,(x,k)->x+k);
//    }
    	
     private static int addNumber(List<Integer> number) {
    	
    	return number.stream().reduce(0,Integer::sum);
    }

 	
}




